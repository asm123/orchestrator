package com.cloud.storage;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.AbstractMap;
import java.util.Map.Entry;

import org.libvirt.LibvirtException;

import com.cloud.compute.VM;
import com.cloud.global.Global;
import com.cloud.global.Host;
import com.cloud.utilities.Log;


public class Volume 
{
	public int volumeid;
	public String volumeName;
	public int volumeSize;
	int vmid = 0;
	String target = new String ();
	
	public Volume ()
	{
		volumeid = 0;
		vmid = 0;
	}
	
	public Volume (String name, int size)
	{
		this.volumeName = name;
		this.volumeSize = size * 1024;
		this.volumeid = 0;
		this.vmid = 0;
	}
	
	public int create ()
	{
		String command = "sudo rbd create " + volumeName 
				+ " --size " + volumeSize
				+ " -m " + Global.monitorIP
				+ " -k " + Global.keyring;
		boolean created = executeCommand (command);
		if (created)
		{
			this.volumeid = Global.volumeid++;
			Global.volumeIdToVolume.put (volumeid, this);
			Global.volumeIdToVolumeName.put (volumeid, volumeName);
			return volumeid;
		}
		return 0;
	}
	
	public int destroy ()
	{
		boolean isAttached = Global.attachedVolumes.containsKey (volumeid);
		if (isAttached)
			return 0;
		String command = "sudo rbd rm " + volumeName + " -m " + Global.monitorIP 
				+ " -k " + Global.keyring;
		boolean destroyed = executeCommand (command);
		if (destroyed)
		{
			Global.volumeIdToVolumeName.remove (volumeid);
			Global.volumeIdToVolume.remove (volumeid);
			Global.attachedVolumes.remove (volumeid);
			return 1;
		}
		return 0;
	}
		
	public int attach (int vmid)
	{	
		try 
		{
			Log.info ("Trying to attach volume " + volumeid + " to vm " + vmid);
			boolean isAttached = Global.attachedVolumes.containsKey (volumeid);
			if (isAttached)
			{
				Log.error ("Volume " + volumeid + " is already attached to vm " 
						+ Global.attachedVolumes.get (volumeid));
				return 0;
			}
			Log.info ("Volume " + volumeid + " is not attached to any VM!");
			VM vm = Global.vmidToVM.get (vmid);
			if (vm == null)
			{
				Log.error ("VM with " + vmid + " does not exist.");
				return 0;
			}
			
			int temp = getTarget (vm);
			if (temp == -1)
				return 0;
			target = "sd" + (char) temp;
			Log.info ("Target: " + target);
			Host host = Global.pmidToPM.get (vm.pmid);
			String uuid = host.connect.listSecrets()[0];
			System.out.println ("UUID: " + uuid);
			
			String deviceXML = "<disk type='network' device='disk'>"
					+ "<source protocol='rbd' name='rbd/" + this.volumeName + "'>"
					+ "<host name='" + Global.monitorName + "' port='6789'/>"
					+ "</source>"
					+ "<auth username='admin'>"
					+ "<secret type='ceph' " 
					+ "uuid='" + uuid + "'/>"
					+ "</auth>"
					+ "<target dev='sd" + (char) temp + "' bus='scsi'/>"
					+ "</disk>"; 
			Log.info ("Attaching device...");
			vm.domain.attachDevice (deviceXML);
			Log.success ("Disk attached successfully!");
			Entry <Integer, String> device = 
					new AbstractMap.SimpleEntry <Integer, String> (vmid, deviceXML);
			this.vmid = vmid;
			Global.attachedVolumes.put (volumeid, device);
			vm.targetToVolume.put (target, volumeid);
			Log.success ("Disk " + this.volumeName + " attached to " + vm.vmName);
			return 1;
		} 
		catch (LibvirtException e) 
		{
			System.out.println ("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println ("Disk attach failed!");
		return 0;
	}
	
	public int detach ()
	{
		System.out.println ("in volume detach");
		
		VM vm = Global.vmidToVM.get (vmid);
		if (vm == null)
		{
			System.out.println ("VM with " + vmid + " does not exist.");
			return 0;
		}
		System.out.println ("detach vm: " + vmid);
		
		if (Global.attachedVolumes.containsKey (this.volumeid))
		{
			String deviceXML = Global.attachedVolumes.get (this.volumeid).getValue();
			System.out.println ("device xml in detach: " + deviceXML);
			try 
			{
				Log.info ("Detaching device...");
				vm.domain.detachDevice (deviceXML);
				Log.success ("Device detached successfully!");
				Global.attachedVolumes.remove (volumeid);
				vm.targetToVolume.put (target, volumeid);
				return 1;
			} 
			catch (LibvirtException e) 
			{
				Log.error ("Exception: " + e.getMessage());
				e.printStackTrace();
				return 0;
			}
		}
		else
		{
			System.out.println ("in detach: nothing attached");
			return 1;
		}
		
		
	}
		
	private int getTarget (VM vm)
	{
		if (vm.currentTarget == 123)
			return -1;
		return vm.currentTarget++;
	}
	
	private boolean executeCommand (String command)
	{
		try
		{
			Runtime rt = Runtime.getRuntime();
	        java.lang.Process p = rt.exec (command);
	        p.waitFor();
	        System.out.println ("Process exited with code = " + p.exitValue());
	        InputStream is = p.getInputStream();
	        BufferedReader reader = new BufferedReader (new InputStreamReader (is));
	        String s = null;
	        while ((s = reader.readLine()) != null) 
	        {
	            System.out.println (s);
	        }
	        is.close();
			
	        if (p.exitValue() != 0)
			{
				Log.error ("Command " + command + " exited with status " + p.exitValue());
				return false;
			}
			return true;
		}
		catch (Exception e)
		{
			Log.error ("Exception: " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}
}
