package com.cloud.storage;

import com.cloud.global.Global;
import com.cloud.utilities.VMJSONResponse;
import com.cloud.utilities.VolumeJSONResponse;

public class VolumeManager 
{
	public static String create (String volumeName, int size)
	{
		Volume volume = new Volume (volumeName, size);
		int volumeid = volume.create ();
		return VolumeJSONResponse.create (volumeid);
	}
	
	public static String query (int volumeid)
	{
		return VolumeJSONResponse.query (volumeid);
	}
	
	public static String destroy (int volumeid)
	{
		int status;
		Volume volume = Global.volumeIdToVolume.get (volumeid);
		if (volume == null)
			status = 0;
		else
			status = volume.destroy();
		return VMJSONResponse.destroy (status);
	}
	
	public static String attach (int vmid, int volumeid)
	{
		int status;
		Volume volume = Global.volumeIdToVolume.get (volumeid);
		if (volume == null)
			status = 0;
		else
			status = volume.attach (vmid);
		return VMJSONResponse.destroy (status);
	}
	
	public static String detach (int volumeid)
	{
		int status;
		Volume volume = Global.volumeIdToVolume.get (volumeid);
		if (volume == null)
			status = 0;
		else
			status = volume.detach();
		return VMJSONResponse.destroy (status);
	}

	public static String list () 
	{
		return VolumeJSONResponse.list ();
	}
}