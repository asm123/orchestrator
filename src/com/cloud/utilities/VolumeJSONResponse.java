package com.cloud.utilities;

import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.cloud.global.Global;
import com.cloud.storage.Volume;

public class VolumeJSONResponse 
{
	static JSONObject obj;
	
	@SuppressWarnings("unchecked")
	public static String create (int volumeid)
	{   	 
   	 	obj = new JSONObject();
   	 	obj.put ("volumeid", volumeid);
   	 	return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String query (int volumeid)
	{
		obj = new JSONObject ();
		
		Volume volume = Global.volumeIdToVolume.get (volumeid);
		System.out.println ("In volume: " + volumeid);
		if (volume == null)
		{
			obj.put ("error", "volumeid: " + volumeid + " does not exist");
		}
		else
		{
			obj.put ("volumeid", volume.volumeid);
			obj.put ("name", volume.volumeName);
			obj.put ("size", volume.volumeSize / 1024);
			if (Global.attachedVolumes.containsKey (volumeid))
			{
				obj.put ("status", "attached");
				obj.put ("vmid", Global.attachedVolumes.get (volumeid).getKey());
			}
			else
				obj.put ("status", "available");
		}
		
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String attach (int status)
	{
		obj = new JSONObject ();
		obj.put ("status", status);
		
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String detach (int status)
	{
		obj = new JSONObject ();
		obj.put ("status", status);
		
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public static String destroy (int status)
	{
		obj = new JSONObject ();
		obj.put ("status", status);
		
		return obj.toJSONString();
	}

	@SuppressWarnings("unchecked")
	public static String list () 
	{
		JSONObject root = new JSONObject();
		
		JSONArray arr = new JSONArray();
		
		for (Entry <Integer, String> entry : Global.volumeIdToVolumeName.entrySet())
		{
			JSONObject obj = new JSONObject();
			
			obj.put ("volumeid", entry.getKey());
			obj.put ("name", entry.getValue());
   	     	arr.add (obj);
   	 	}
		root.put ("volume", arr);
		
		return root.toJSONString();
	}
}
