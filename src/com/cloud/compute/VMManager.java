package com.cloud.compute;

import com.cloud.global.Global;
import com.cloud.utilities.VMJSONResponse;

public class VMManager 
{
	public static String create (String vmName, int instanceType, int imageID)
	{
		VM vm = new VM ();
		int vmid = vm.create (vmName, instanceType, imageID);
		return VMJSONResponse.create (vmid);
	}
	
	public static String query (int vmid)
	{
		return VMJSONResponse.query (vmid);
	}
	
	public static String destroy (int vmid)
	{
		System.out.println ("destroy in vmmanager: vmid: " + vmid);
		
		int status;
		VM vm = Global.vmidToVM.get (vmid);
		if (vm == null)
			status = 0;
		else
			status = vm.destroy();
		System.out.println ("destroy in vmmanager: status:  " + status);
		return VMJSONResponse.destroy (status);
	}
	
	public static String types ()
	{
		return VMJSONResponse.types ();
	}
	
	public static String images ()
	{
		return VMJSONResponse.images ();
	}

	public static String list() 
	{
		return VMJSONResponse.list ();
	}

	public static String queryImage (int image_id) 
	{
		return VMJSONResponse.queryImage (image_id);
	}
}
