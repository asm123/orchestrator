package com.cloud.compute;
import java.util.ArrayList;
import java.util.List;

public class Capabilities 
{
    public List <Capabilities.Guest> guest;
    
    public Capabilities ()
    {
    	guest = new ArrayList <Capabilities.Guest> ();
    }
    
    public List <Capabilities.Guest> getGuest() 
    {
        if (guest == null) 
        {
            guest = new ArrayList <Capabilities.Guest> ();
        }
        return this.guest;
    }

    public static class Guest 
    {
        protected String osType;
        public Capabilities.Guest.Arch arch;

        public Guest ()
        {
        	osType = new String ("hvm");
        	arch = new Capabilities.Guest.Arch();
        }
        
        public String getOsType() 
        {
            return osType;
        }

        public void setOsType (String value) 
        {
            this.osType = value;
        }

        public Capabilities.Guest.Arch getArch() 
        {
            return arch;
        }

        public void setArch (Capabilities.Guest.Arch value) 
        {
            this.arch = value;
        }

        public static class Arch 
        {
            protected byte wordSize;
            protected String emulator;
            public List <String> machine;
            public List <Capabilities.Guest.Arch.Domain> domain;
            public String name;

            public Arch ()
            {
            	wordSize = 32;
            	emulator = new String ("/usr/bin/qemu");
            	machine = new ArrayList <String> ();
            	domain = new ArrayList <Capabilities.Guest.Arch.Domain> ();
            	name = new String ("i686");
            }
            
            public byte getWordSize() 
            {
                return wordSize;
            }

            public void setWordSize (byte value) 
            {
                this.wordSize = value;
            }

            public String getEmulator() 
            {
                return emulator;
            }

            public void setEmulator (String value) 
            {
                this.emulator = value;
            }

            public List <String> getMachine() 
            {
                if (machine == null)
                    machine = new ArrayList<String>();
                return this.machine;
            }

            public List <Capabilities.Guest.Arch.Domain> getDomain() 
            {
                if (domain == null)
                    domain = new ArrayList <Capabilities.Guest.Arch.Domain>();
                return this.domain;
            }

            public String getName() 
            {
                return name;
            }

            public void setName (String value) 
            {
                this.name = value;
            }


            public static class Domain 
            {
                protected String type;
                
                public Domain ()
                {
                	type = new String ("qemu");
                }
                
                public String getType() 
                {
                    return type;
                }

                public void setType (String value) 
                {
                    this.type = value;
                }
            }

        }
    }
}