package com.cloud.compute;

public class InstanceType 
{
	public int tid;
	public long cpu;
	public long ram;
	public long disk;
	
	public InstanceType (int tid, long cpu, long ram, long disk)
	{
		this.tid = tid;
		this.cpu = cpu;
		this.ram = ram * 1024;
		this.disk = disk;
	}
}
