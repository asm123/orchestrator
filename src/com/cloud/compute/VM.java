package com.cloud.compute;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import org.libvirt.Connect;
import org.libvirt.Domain;
import org.libvirt.LibvirtException;

import com.cloud.global.Global;
import com.cloud.global.Host;
import com.cloud.storage.Volume;

public class VM 
{
	public DomainConfig dcfg;
	public Domain domain;
	public int vmid;
	public int pmid;
	public String vmName;
	public int instanceType;
	public String imageName;
//	public HashSet <String> usedTargets = new HashSet <String> ();
	public int currentTarget = 97;
	public HashMap <String, Integer> targetToVolume = new HashMap <String, Integer> ();
	
	public VM ()
	{
		vmid = 0;
		pmid = -1;
	}
	
	public int create (String vmName, int instanceType, int imageID)
	{
		this.vmName = vmName;
		this.instanceType = instanceType;
		
		if (imageID >= Global.images.size())
			return 0;
		this.imageName = Global.images.get (imageID);
		
		getHost (instanceType, imageName);
		if (pmid == 0)
		{
			System.out.println ("Could not get a host.");
			return 0;
		}
		try 
		{
			System.out.println ("host: " + pmid + " name: " + Global.pmidToPM.get (pmid).connect.getURI());
		} 
		catch (LibvirtException e) 
		{
			System.out.println ("Error in creation!");
			e.printStackTrace();
			return 0;
			
		}		
		
		Global.currentPM = this.pmid;
		
		boolean status = configureDomain () && createDomain ();
		if (status)
		{
			System.out.println ("VM created successfully! vmid: " + vmid + " pmid: " + pmid);
			return vmid;
		}
		return 0;			
	}
	
	public boolean configureDomain ()
	{		
		// configure domain
		
		long vcpu = Global.instanceType.get (instanceType).cpu;
		Long ram = Global.instanceType.get (instanceType).ram;
		
		Host host = Global.pmidToPM.get (pmid);
		dcfg = new DomainConfig ();
		boolean status = dcfg.defineDomainConfig (host, vmName, vcpu, ram, imageName);
		System.out.println ("Domain configured: " + status);
		return status;
	}
	
	public boolean createDomain ()
	{
		try 
		{
			String xmlString = dcfg.toXMLString ();
			Connect connect = Global.pmidToPM.get (pmid).connect;
			domain = connect.domainDefineXML (xmlString);
			
			int creationStatus = domain.create ();
			
			System.out.println ("VM creation status: " + creationStatus);
			
			if (creationStatus == -1)
			{
				domain.undefine();
				vmid = 0;
				return false;
			}
			
			vmid = Global.vmid++;
			Global.vmidToVM.put (vmid, this);
			Global.vmNameTovmid.put (dcfg.name, vmid);
			Global.vmToPm.put (vmid, pmid);
			Host host = Global.pmidToPM.get (pmid);
			
			HashSet <Integer> vms = Global.pmToVm.get (pmid);
			if (vms == null)
				vms = new HashSet <Integer> ();
			vms.add (vmid);
			Global.pmToVm.put (pmid, vms);
			host.resources.ram = host.resources.ram - dcfg.memory;
			host.resources.cpu = host.resources.cpu - dcfg.vcpu;
			System.out.println ("configured at " + host.ip);
		} 
		catch (LibvirtException e) 
		{
			System.out.println ("Error in createDomain");
			e.printStackTrace();
			if (domain == null)
			{
				System.out.println ("Error: Domain " + dcfg.name + " already exists!");
			}
			else
			{
				try 
				{
					domain.undefine();
				} 
				catch (LibvirtException e1) 
				{
					e1.printStackTrace();
				}
			}
			vmid = 0;
			return false;
		}
		return true;
	}
	
	public int destroy ()
	{
		System.out.println ("In vm destroy");
		try 
		{
			System.out.println ("going to targetToVolume");			
			for (Entry <String, Integer> entry : targetToVolume.entrySet())
			{
				System.out.println ("in targetToVolume: " + entry.getKey() + " val: " + entry.getValue());
				int volumeid = entry.getValue ();
				System.out.println ("going to targetToVolume vol id: " + volumeid );
				if (volumeid != 0)
				{
					Volume volume = Global.volumeIdToVolume.get (volumeid);
					if (volume == null)
						System.out.println ("volume is null");
					else
					{
						System.out.println ("volume is: " + volume.volumeid);
						int status = volume.detach();
						System.out.println ("vol detach status: " + status);
					}
				}
			}
			
			System.out.println ("Destroying vmid: " + vmid);
			
			domain.destroy();
			System.out.println ("Undefining vmid: " + vmid);
			domain.undefine();
			Global.vmToPm.remove (vmid);
			Global.vmNameTovmid.remove (vmName);
			Global.vmidToVM.remove (vmid);
			Global.pmToVm.get (pmid).remove (vmid);
			System.out.println ("vmid: " + vmid + " :" + Global.vmidToVM.get (vmid));
			Host host = Global.pmidToPM.get (pmid);
			host.resources.ram = host.resources.ram + dcfg.memory;
			host.resources.cpu = host.resources.cpu + dcfg.vcpu;
		} 
		catch (LibvirtException e) 
		{
			System.out.println ("Error in destroyDomain");
			return 0;
		}
		return 1;
	}
	
	private void getHost (int instanceType, String imageName)
	{
		long vcpu = Global.instanceType.get (instanceType).cpu;
		Long ram = Global.instanceType.get (instanceType).ram;
//		Long disk = Global.instanceType.get (instanceType).disk;
		
		boolean is64 = imageName.contains ("64");
		
		for (Entry <Integer, Host> entry : Global.pmidToPM.entrySet())
		{
			Host host = entry.getValue();
			System.out.println ("Host: " + host.hostName);
			if (host.connect == null)
			{
				System.out.println ("Connect null");
				continue;
			}
			
			System.out.println ("Required cpu: " + vcpu + " ram: " + ram);
			System.out.println ("Got machine: " + host.ip + " cpu: " + host.resources.cpu + " ram: " + host.resources.ram);
			
			if (host.resources.cpu >= vcpu && host.resources.ram >= ram)
			{
				if (is64)
				{
					System.out.println ("64-bit");
					if (host.is64)
					{
						this.pmid = host.pmid;
						break;
					}
				}
				else
				{
					System.out.println ("32-bit");
					this.pmid = host.pmid;
					break;
				}
			}
		}
		System.out.println ("pmid: " + this.pmid);
	}
}
