package com.cloud.rest;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.cloud.storage.VolumeManager;

public class AttachVolumeHandler extends AbstractHandler 
{
	@Override
	public void handle (String target, Request baseRequest, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException 
	{
		// settings
		
		response.setContentType ("application/json;charset=utf-8");
		response.setStatus (HttpServletResponse.SC_OK);
		baseRequest.setHandled (true);
		
		// take parameters
		
		int vmid = Integer.parseInt (baseRequest.getParameter ("vmid"));
		int volumeid = Integer.parseInt (baseRequest.getParameter ("volumeid"));
		
		System.out.println ("Volume id: " + volumeid + " vmid: " + vmid);
		String resp = VolumeManager.attach (vmid, volumeid);
		response.getWriter ().write (resp);
	}
}
