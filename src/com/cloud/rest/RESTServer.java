package com.cloud.rest;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;

import com.cloud.global.Global;

public class RESTServer
{	
	public void run ()
	{
		Server server = new Server (Global.restServerPort);
		
		// VM handlers
		
		ContextHandler createVM = new ContextHandler ();
		createVM.setContextPath ("/vm/create");
		createVM.setHandler (new CreateVMHandler ());
		
		ContextHandler queryVM = new ContextHandler ();
		queryVM.setContextPath ("/vm/query");
		queryVM.setHandler (new QueryVMHandler ());
		
		ContextHandler destroyVM = new ContextHandler ();
		destroyVM.setContextPath ("/vm/destroy");
		destroyVM.setHandler (new DestroyVMHandler ());
		
		ContextHandler types = new ContextHandler ();
		types.setContextPath ("/vm/types");
		types.setHandler (new TypesHandler ());
		
		ContextHandler listVM = new ContextHandler ();
		listVM.setContextPath ("/vm/list");
		listVM.setHandler (new ListVMHandler ());
		
		ContextHandler list = new ContextHandler ();
		list.setContextPath ("/image/list");
		list.setHandler (new ImagesHandler ());
		
		// Volume handlers
		
		ContextHandler createVolume = new ContextHandler ();
		createVolume.setContextPath ("/volume/create");
		createVolume.setHandler (new CreateVolumeHandler ());
		
		ContextHandler queryVolume = new ContextHandler ();
		queryVolume.setContextPath ("/volume/query");
		queryVolume.setHandler (new QueryVolumeHandler ());
		
		ContextHandler destroyVolume = new ContextHandler ();
		destroyVolume.setContextPath ("/volume/destroy");
		destroyVolume.setHandler (new DestroyVolumeHandler ());
		
		ContextHandler attachVolume = new ContextHandler ();
		attachVolume.setContextPath ("/volume/attach");
		attachVolume.setHandler (new AttachVolumeHandler ());
		
		ContextHandler detachVolume = new ContextHandler ();
		detachVolume.setContextPath ("/volume/detach");
		detachVolume.setHandler (new DetachVolumeHandler ());
		
		ContextHandler listVolume = new ContextHandler ();
		listVolume.setContextPath ("/volume/list");
		listVolume.setHandler (new ListVolumeHandler ());
		
		ContextHandlerCollection collection = new ContextHandlerCollection ();
		ContextHandler [] contextualHandles = {createVM, queryVM, destroyVM, listVM, types, list,
				createVolume, queryVolume, destroyVolume, attachVolume, detachVolume, listVolume};
		collection.setHandlers (contextualHandles);
		
		server.setHandler (collection);
	
		try 
		{
			server.start();
			server.join();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
	}
}
